from __future__ import print_function
from time import time

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.datasets import fetch_20newsgroups

import numpy

n_samples = 200
n_features = 20
n_topics = 3
n_top_words = 2


dicto = {1:'a', 2:'b', 3:'c', 4:'d', 5:'e', 6:'f', 7:'g', 8:'h', 9:'i', 10:'j',
        11:'k', 12:'l', 13:'m', 14:'n', 15:'o', 16:'p', 17:'q', 18:'r', 19:'s', 20:'t'}

# def print_top_words(model, feature_names, n_top_words):
#     for topic_idx, topic in enumerate(model.components_):
#         print("Topic #%d:" % topic_idx)
#         print(" ".join([feature_names[i]
#                         for i in topic.argsort()[:-n_top_words - 1:-1]]))
    # print()

# def print_top_words(model, feature_names, n_top_words):
#     for topic_id, topic in enumerate(model.components_):
#         print('\nTopic Nr.%d:' % int(topic_id + 1))
#         print(''.join([feature_names[i] + ' ' + str(round(topic[i], 2))
#               +' | ' for i in topic.argsort()[:-n_top_words - 1:-1]]))


print("Loading dataset...")
'''
TODO: load your own dataset
'''
# dataset = fetch_20newsgroups(shuffle=True, random_state=1,
#                              remove=('headers', 'footers', 'quotes'))
# data_samples = dataset.data[:n_samples]
# print (data_samples[1:3])

text_file = open("test_output_f.txt", "r")
lines = text_file.read().split('\n')
# print (lines)
docs = []
for i in range(200):
    splits = lines[i].split()
    # print (splits)
    doc = []
    for j in range(len(splits)):
        temp = splits[j].split(':')
        # print (temp)
        doc.append(dicto[int(temp[0])+1]*int(temp[1]))
    # doc = ''.join(doc)
    # doc = ' '.join(doc)
    docs.append(''.join(doc))
    # print (docs)
data_samples = docs
print (data_samples)


# Use tf (raw term count) features for LDA.
print("Extracting tf features for LDA...")
tf_vectorizer = CountVectorizer(max_df=1, min_df=1,
                                max_features=n_features,
                                stop_words='english')

tf = tf_vectorizer.fit_transform(data_samples)

print("Fitting LDA models with tf features, "
      "n_samples=%d and n_features=%d..."
      % (n_samples, n_features))
lda = LatentDirichletAllocation(n_topics=n_topics, max_iter=5,
                                learning_method='batch',
                                random_state=0)
lda.fit(tf)

# print("\nTopics in LDA model:")
tf_feature_names = tf_vectorizer.get_feature_names()
# print_top_words(lda, tf_feature_names, n_top_words)

## topic word matrix OR P(W|Z)
# print (lda.components_) # unnormalized values (counts)
total_w_per_t = numpy.sum(lda.components_, axis=1)
for i in range(3): lda.components_[i][:] = lda.components_[i][:]/total_w_per_t[i]

print ('\n', 'Topic word matrix,normalized value)', '\n')
print (lda.components_)

sum=0
for topic in range(lda.components_.shape[0]):
    for word in range(lda.components_.shape[1]):
        sum += lda.components_[topic][word] * numpy.log(lda.components_[topic][word])
print ('entropy for word topic dist', sum/lda.components_.shape[0] * -1)

## Transformation on the data to get p(t|d) counts
doc_topic = lda.transform(tf)
# print (doc_topic)
sum=0
for doc in range(doc_topic.shape[0]):
    for topic in range(doc_topic.shape[1]):
        sum += doc_topic[doc][topic] * numpy.log(doc_topic[doc][topic])
print ('entropy for topic doc dist', sum/doc_topic.shape[0] * -1)

## List the doc and its highest score topic
# for n in range(doc_topic.shape[0]):
#     topic_most_pr = doc_topic[n].argmax()
#     print("doc: {} topic: {}\n".format(n,topic_most_pr))
