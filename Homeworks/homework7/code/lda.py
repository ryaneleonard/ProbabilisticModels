import math
import random
import numpy
import numpy.random
import sys

## define some constant
TOPIC_N = 3
VOCABULARY_SIZE = 20
DOC_NUM = 200
TERM_PER_DOC = 50

beta = [0.01 for i in range(VOCABULARY_SIZE)]
alpha = [0.1 for i in range(TOPIC_N)]
FILE_NAME = sys.argv[1]

phi = []
## generate multinomial distribution over words for each topic
for i in range(TOPIC_N):
    topic = numpy.random.mtrand.dirichlet(beta, size = 1)
    phi.append(topic)

## generate words for each document
output_f = open(FILE_NAME+'_output_f.txt','w')
z_f = open(FILE_NAME+'_z_f.txt','w')
theta_f = open(FILE_NAME+'_theta_f.txt','w')

p_w_z = numpy.zeros((TOPIC_N,VOCABULARY_SIZE)) # matrix to store p(word|topic)
p_z_d = numpy.zeros((DOC_NUM, TOPIC_N)) # matrix to store p(topic|Document)

for i in range(DOC_NUM):
    buffer = {}
    z_buffer = {} ## keep track the true z
    ## first sample theta
    theta = numpy.random.mtrand.dirichlet(alpha,size=1)
    for j in range(TERM_PER_DOC):
        ## first sample z
        z = numpy.random.multinomial(1,theta[0],size=1)
        z_assignment = 0
        for k in range(TOPIC_N):
            if z[0][k] == 1:
                break
            z_assignment += 1
        if not z_assignment in z_buffer:
            z_buffer[z_assignment] = 0
        z_buffer[z_assignment] = z_buffer[z_assignment] + 1
        ## sample a word from topic z
        w = numpy.random.multinomial(1,phi[z_assignment][0],size = 1)
        w_assignment = 0
        for k in range(VOCABULARY_SIZE):
            if w[0][k] == 1:
                break
            w_assignment += 1

        p_w_z[z_assignment][w_assignment] += 1

        if not w_assignment in buffer:
            buffer[w_assignment] = 0
        buffer[w_assignment] = buffer[w_assignment] + 1

    ## output
    # output_f.write(str(i)+'\t'+str(TERM_PER_DOC)+'\t')
    for word_id, word_count in buffer.items():
        output_f.write(str(word_id)+':'+str(word_count)+' ')
    output_f.write('\n')

    # z_f.write(str(i)+'\t'+str(TERM_PER_DOC)+'\t')
    for z_id, z_count in z_buffer.items():
        z_f.write(str(z_id)+':'+str(z_count)+' ')
        # print (z_id, z_count)
        p_z_d[i][z_id] = z_count
    z_f.write('\n')


    theta_f.write(str(i)+'\t')
    for k in range(TOPIC_N):
        theta_f.write(str(k)+':'+str(theta[0][k])+' ')
    theta_f.write('\n')

z_f.close()
theta_f.close()
output_f.close()

# print (p_w_z)
## normalizing to get probabilites from counts
total_w_per_t = numpy.sum(p_w_z, axis=1)
for i in range(3): p_w_z[i][:] = p_w_z[i][:]/total_w_per_t[i]
# print ('\n','Topic word matrix (normalized value)','\n',p_w_z)

## output phi
output_f = open(FILE_NAME+'_phi.txt','w')
for i in range(TOPIC_N):
    output_f.write(str(i)+'\t')
    for j in range(VOCABULARY_SIZE):
        output_f.write(str(j)+':'+str(phi[i][0][j])+' ')
    output_f.write('\n')
output_f.close()

# print (p_z_d)
## normalizing to get probabilites from counts
# total_t_per_t = numpy.sum(p_z_d, axis=1)
for i in range(200): p_z_d[i][:] = p_z_d[i][:]/50
print (p_z_d)  ## topic document prob matrix
