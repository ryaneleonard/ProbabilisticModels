\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}

\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass\ (\hmwkClassInstructor\ \hmwkClassTime): \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Task \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework\ \#1}
\newcommand{\hmwkDueDate}{February 1}
\newcommand{\hmwkClass}{Probabilistic Models}
\newcommand{\hmwkClassTime}{}
\newcommand{\hmwkClassInstructor}{Michael Mozer}
\newcommand{\hmwkAuthorName}{\textbf{Ryan Leonard} }

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}


%	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Problem4.jpg}\\

\begin{document}

\maketitle

\pagebreak

\section{Goal}
The goal of this assignment is to give you a concrete understanding of the Tenenbaum (1999) work by implementing and experimenting with a small scale version of the model applied to learning concepts in two-dimensional spaces.  The further goal is to get hands-on experience representing and manipulating probability distributions and using Bayes' rule.\\

\section{Simplified Model}
Consider a two-dimensional input space with features in the range [-10, 10]. We will consider only square concepts, and concepts centered on the origin (0,0).  We will also consider only a discrete set of concepts, $H = \{h_i, i=1 .. .10\}$, where $h_i$ is the concept with lower left corner (-i, -i) and upper right corner (+i, +i), i.e., a square with the length of each side being 2i.

You will have to define a discrete prior distribution over the 10 hypotheses, and you will have to do prediction by marginalizing over the hypothesis space.  Use Tenenbaum's expected-size prior.  Because the expected size prior is defined over a continuous distribution, you will need to compute the value of the prior for each of the 10 hypotheses, and renormalize the resulting probabilities so that the prior distribution sums to 1.  (You don't actually need to do this renormalization, because the normalization factor cancels out when you do the Bayesian calculations, but go ahead and do it anyhow, just to have a clean representation of the priors.)\\

\section{Note To Grader}
- For each of the homework problems below, a probability is reported for each hypothesis. Each of these values is reported from left to right in relation to increasing values of i.\\
- In problems 3-5, the numerous lines of the contour plot do not directly represent the true contours of the plot. I spoke with Shirly about this visualization, and we agreed during office hours that the plot would be sufficient when accompanied with the probability values of the true contours.

\begin{homeworkProblem}
Make a bar graph of the prior distribution, P(H), for $\sigma_1 = \sigma_2 = 6$.  Make a graph of the prior distribution for $\sigma_1 = \sigma_2 = 12$. \\

\textbf{Solution:}\\
		\includegraphics[width=.5\textwidth, height=.5\textheight, keepaspectratio]{problem1sigma6.png}\\
		Probability Values:
	$	[4.87203e-01,
		2.50138e-01,
		1.28425e-01,
		6.59357e-02,
		3.38525e-02,
		1.73805e-02,
		8.92343e-03,
		4.58144e-03,
		2.35219e-03,
		1.20766e-03]$
		
		
		\includegraphics[width=.5\textwidth, height=.5\textheight, keepaspectratio]{problem1sigma12.png}\\
		Probability values:
$[2.93955e-01,
2.10628e-01,
1.50922e-01,
1.08140e-01,
7.74858e-02,
5.55210e-02,
3.97825e-02,
2.85054e-02,
2.04250e-02,
1.46352e-02]$

	

\textbf{Math:}\\
The probability for each value of i was computed as follows:
\begin{align}
P(H) &= e^{-\left(\frac{s_1}{\sigma_1} + \frac{s_2}{\sigma_2}\right)}\\
&= e^{-\left(\frac{2i}{\sigma_1} + \frac{2i}{\sigma_2}\right)}\\
&= e^{-\left(\frac{2i}{\sigma_1} + \frac{2i}{\sigma_1}\right)}\\
&= e^{-\left(\frac{4i}{\sigma_1}\right)}
\end{align}
Step 2 arises from working with a square hypothesis area as described in the simplified model. \\
Step 3 was made from the problem statement defining $\sigma_1 = \sigma_2$

\end{homeworkProblem}


\begin{homeworkProblem}
Given one observation, $X = \{(1.5, 0.5)\}$, compute the posterior $P(H \vert X)$ with $\sigma = 12$. You will get one probability for each possible hypothesis. Display your result either as a bar graph or a list of probabilities. Use Tenenbaum's Size Principle as the likelihood function. \\

\textbf{Solution:}\\
	\includegraphics[width=.5\textwidth, height=.5\textheight, keepaspectratio]{task2.png}\\
	
Probability values:\\
$[0.00000e+00,
6.38404e-01,
2.03305e-01,
8.19419e-02,
3.75769e-02,
1.86979e-02,
9.84317e-03,
5.39991e-03,
3.05715e-03,
1.77434e-03]$


Tenenbaum's size principle states:\\
\begin{align*}
P(X \vert h) &= \frac{1}{\vert h \vert ^n} \text{ if }\forall j, x^{(j)} \in h\\
&= 0 \text{  otherwise}
\end{align*}

From Bayes Rule, we have $$P(H \vert X) = \frac{P(X \vert H) P(H)}{P(X)}$$
Thus, using the size principle, we have for each hypothesis:
\setcounter{equation}{0}
\begin{align}
P(H \vert X) &= \int_{h:y\in h} \frac{\frac{1}{\vert h \vert^n}p(h)}{P(X)}dh\\
&= \int_{h:y\in h} \frac{\frac{1}{\vert (2i)^2 \vert^1}p(h)}{P(X)}dh\\
&\widetilde{  }  \int_{h:y\in h} \frac{1}{\vert (2i)^2 \vert}p(h)dh
\end{align}
Note that in the case of i = 1, we observe that it is not possible for the point $X = \{(1.5, 0.5)\}$ to lie within the bounds of our hypothesis. Therefor, the likelihood $P(X \vert h(i = 1)) = 0$ (Please forgive the slight abuse of notation) Additionally, the denominator P(X) is constant among all probabilities calculated, and is therefore unnecessary to compute.  Finally, in order to compute the final values, we use the prior probabilities computed in task 1.



\end{homeworkProblem}


\begin{homeworkProblem}
\textrm{\huge The contour plots in all following problems do not use a log scale}\\
Using the results of Task 2, compute generalization predictions, $P(y \in concept \vert X)$, over the whole space of possible generalization points, y,  for $X = \{(1.5, 0.5)\}$ and $\sigma = 10$. (I used the notation $P(Q \vert X)$ in the lectures slides.) The input space should span the region from (-10,-10) to (+10,+10). Display your result as a contour map in 2D space where the coloring of the contour map represents the probability that an input at that point in the space will be a member of the concept. (If the probabilities span a wide dynamic range, you can always plot the logarithm of the probability in the contour map. If you choose to do this, be sure to label the graph.)\\

\textbf{Solution:}\\
	\includegraphics[width=.75\textwidth, height=.75\textheight, keepaspectratio]{task3.png}\\
	The probabilities associated with each contour are as follows:\\
	
$[0.00000e+00,
6.65678e-01,
1.98319e-01,
7.47771e-02,
3.20797e-02,
1.49331e-02,
7.35426e-03,
3.77431e-03,
1.99901e-03,
1.08538e-03]$


\end{homeworkProblem}

\begin{homeworkProblem}
Repeat Task 3 for $X = \{(4.5, 2.5)\}$.\\

\textbf{Solution:}\\
\includegraphics[width=.75\textwidth, height=.75\textheight, keepaspectratio]{task4.png}\\
	The probabilities associated with each contour are as follows:\\
$[0.00000e+00,
0.00000e+00,
0.00000e+00,
0.00000e+00,
5.23958e-01,
2.43902e-01,
1.20117e-01,
6.16457e-02,
3.26498e-02,
1.77275e-02]$




\end{homeworkProblem}

\begin{homeworkProblem}
Compute generalization predictions, $P(y\in concept \vert X)$, over the whole input space for $\sigma = 30$ and three different sets of input examples: $X = \{(2.2, -.2)\}$, $X = \{(2.2, -.2), (.5, .5)\}$, and $X = \{(2.2, -.2), (.5, .5), (1.5, 1)\}$. Describe how the posterior is changing as new examples are added, and explain why this occurs.\\

\textbf{Solution:}\\

	\includegraphics[width=.75\textwidth, height=.75\textheight, keepaspectratio]{task5_1.png}\\
		The probabilities associated with each contour are as follows:\\\\
$[0.00000e+00,
0.00000e+00,
5.93197e-01,
2.23668e-01,
9.59547e-02,
4.46669e-02,
2.19976e-02,
1.12895e-02,
5.97930e-03,
3.24651e-03]$


		
	\includegraphics[width=.75\textwidth, height=.75\textheight, keepaspectratio]{task5_2.png}\\
	
		The probabilities associated with each contour are as follows:\\
$[0.00000e+00,
0.00000e+00,
7.69082e-01,
1.63117e-01,
4.47860e-02,
1.44777e-02,
5.23835e-03,
2.05830e-03,
8.61353e-04,
3.78820e-04]$


		
	\includegraphics[width=.75\textwidth, height=.75\textheight, keepaspectratio]{task5_3.png}\\
		The probabilities associated with each contour are as follows:\\
$[0.00000e+00,
0.00000e+00,
8.72015e-01,
1.04034e-01,
1.82808e-02,
4.10384e-03,
1.09092e-03,
3.28188e-04,
1.08515e-04,
3.86569e-05]$

		

Observing the contour maps above, we see that as the number of observed points increases, the probability density of the system becomes concentrated around the smallest viable hypothesis. Specifically, hypotheses with a smaller edge length, s, take on higher probability compared to alternative hypotheses.

As the number of observed points increases the likelihood function, $ P(X \vert h) $ decreases based upon Tenenbaum's size principle: $ P( X \vert h ) = \frac{1}{\vert h \vert ^n}$ (or 0 if $X \not \in h$)
Thus, as the number of observed points increases the likelihood decreases exponentially with respect to the size of the hypothesis area. Because of the exponential in the likelihood function, the probabilities of hypotheses with larger area decrease much more quickly than hypotheses with smaller area.



\end{homeworkProblem}

%\begin{homeworkProblem}
%	Do some other interesting experiment with the model.  One possibility would be to extend the model to accommodate negative as well as positive examples.  Another possibility would be to compare generalization surfaces with and without the size principle, and with an uninformative prior (here, uniform would work) compared to the expected-size prior.\\
%	
%	\textbf{Solution:}\\
%	
%	
%\end{homeworkProblem}

\pagebreak



\end{document}
