import random
import numpy as np
 
import itertools
 
import numpy as np
from scipy import linalg
import matplotlib.pyplot as plt
import matplotlib as mpl
 
from sklearn import mixture
 
 
alpha = 0.5
theta = -0.05
 
def probab(n, tables):
    # Gen random number 0~1
    rand = random.random()
 
    p_total = 0
    existing_table = False
 
    for index, count in enumerate(tables):
 
        # prob = count / (n + concentration)
        prob = float(theta + (len(tables) * alpha))/float(n-1 + theta) # eq. from wikipedia
 
        p_total += prob
        if rand < p_total:
            tables[index] += 1
            existing_table = True
            break
 
    # New table!!
    if not existing_table:
         tables.append(1)
 
    # print (tables)
    return (tables)
 
color_iter = itertools.cycle(['navy', 'c', 'cornflowerblue', 'gold',
                              'darkorange'])
 
 
def plot_results(X, Y_, means, covariances, index, title):
    splot = plt.subplot(2, 1, 1 + index)
    for i, (mean, covar, color) in enumerate(zip(
            means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y_ == i):
            continue
        plt.scatter(X[Y_ == i, 0], X[Y_ == i, 1], .8, color=color)
 
        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)
 
    plt.xlim(-9., 5.)
    plt.ylim(-3., 6.)
    plt.xticks(())
    plt.yticks(())
    plt.title(title)
 
 
if __name__ == '__main__':
 
    # First customer always sits at the first table
    #     # To do otherwise would be insanity
    tables = [1]
    for n in range(2,500):
        T = probab(n, tables)
    if len(T) % 2 == 1:
        T.append(1)
    # for index, count in enumerate(T):
    #         print (index, (count/100), count)
    # print ("----")
 
    X = []
    for index, count in enumerate(T):
        X.append(count/100)
 
    X = np.array(X)
    # X = X.reshape(-1,1)
    X = np.reshape(X, (-1, 2))
 
    # # Fit a Dirichlet process Gaussian mixture using five components
    dpgmm = mixture.BayesianGaussianMixture(n_components=X.shape[0],
                                            covariance_type='full').fit(X)
    plot_results(X, dpgmm.predict(X), dpgmm.means_, dpgmm.covariances_, 1,
                 'Bayesian Gaussian Mixture with a Dirichlet process prior')
 
    plt.show()
