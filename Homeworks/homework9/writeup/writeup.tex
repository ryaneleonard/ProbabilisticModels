\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{enumitem}


\usepackage{graphicx,centernot}


\usetikzlibrary{automata,positioning}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass\ (\hmwkClassInstructor\ \hmwkClassTime): \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{
    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
}

\newcommand{\exitProblemHeader}[1]{
    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
    \stepcounter{#1}
    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1}
\nobreak\extramarks{Problem \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{
    \ifnum#1>0
        \setcounter{homeworkProblemCounter}{#1}
    \fi
    \section{Part \arabic{homeworkProblemCounter}}
    \setcounter{partCounter}{1}
    \enterProblemHeader{homeworkProblemCounter}
}{
    \exitProblemHeader{homeworkProblemCounter}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework\ \#9}
\newcommand{\hmwkDueDate}{May 7}
\newcommand{\hmwkClass}{Probabilistic Models}
\newcommand{\hmwkClassTime}{}
\newcommand{\hmwkClassInstructor}{Michael Mozer}
\newcommand{\hmwkAuthorName}{\textbf{Ryan Leonard and Anuj Pasricha} }

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{Due\ on\ \hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
    \vspace{3in}
}

\author{\hmwkAuthorName}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}


% Create a symbol for conditional independence:
% Code found here: https://tex.stackexchange.com/questions/218631/symbol-for-not-conditionally-independent?noredirect=1
\newcommand{\bigCI}{\mathrel{\text{\scalebox{1.07}{$\perp\mkern-10mu\perp$}}}}
\newcommand{\nbigCI}{\centernot{\bigCI}}

\newcommand{\CI}{\mathrel{\perp\mspace{-10mu}\perp}}
\newcommand{\nCI}{\centernot{\CI}}
% End Conditional independence command


%	\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{Problem4.jpg}\\

\begin{document}

\maketitle

\pagebreak



\section{1a:}
Write code to implement a function over which we will optimize. This function takes an input vector and outputs a function value. I'd suggest using a 2-dimensional input space in order that you can easily visualize the function as well as the progress of Bayesian optimization.  For example, your function might be based on a mixture of Gaussians added to a linear function of the inputs. Make the function complex enough that there are some local optima and that the global optimum isn't in one corner of the space. I'd like your function to add noise to the returned value, e.g., Gaussian mean zero noise. Make a plot of the expected value of the function.\\

\textbf{Solution:}\\
We chose to use a mixture of gaussians as our target function. Our function has an easily achievable local maxima of 0.40 at (2, 2), and a harder to achieve global maxima of 0.533 at (-2, -2).\\
Our function is: $$z = 0.5 e^{-\frac{(x+2)^2}{2} + \frac{(y + 2)^2}{2}} + 0.4 e^{-\frac{(x-2)^2}{8} + \frac{(y - 2)^2}{32}}$$

Below, we observe this cost function in both 2d and 3d.\\
\includegraphics[width=0.5\textwidth, height=0.5\textheight, keepaspectratio]{target_function_3d.png} 
\includegraphics[width=0.5\textwidth, height=0.5\textheight, keepaspectratio]{target_function_2d.png}\\




\section{1b:}
Run Bayesian optimization with queries being answered by a call to your function. For the Bayesian optimization, do not build in any knowledge you have about the solution, e.g., allow the optimization procedure to estimate the function mean and observation noise variance.  Describe the assumptions you made in your model and report on the outcome of experiments, e.g., report how close the estimated optimum is from the true optimum as more experiments are performed. To get a reliable estimate of convergence, you'll need to run the optimization process multiple times.

Test at least two different GP covariance functions, and run the optimization process enough times you can determine which covariance function converges most rapidly. Summarize your results in graphs and a few sentences.

Test at least two different acquisition functions (i.e., schemes for performing active selection), and run the optimization procedure enough times that you can determine which acquisition function is more effective. Summarize your results in graphs and a few sentences.\\

\textbf{Solution:}\\
For this section, the following abbreviations are used:\\
UCB = Upper Confidence Bound\\
EI = Expected Improvement\\
POI = Probability of Improvement\\
RBF = Radial Basis Function\\
RQ = Rational Quadratic\\

For our Bayesian Optimization Evaluation, we ran tests using the EI, POI, and UCB acquisition functions using the BayesianOptimization package: https://github.com/fmfn/BayesianOptimization. 

Additionally, we used the sklearn RBF and Rational Quadratic Gaussian Process Kernels.

For each permutation, we ran 10 tests, providing 3 initial starting points, and allowed the optimizer to run for 50 iterations. 

The alpha parameter of our gaussian kernels was set to 1e-5, and the kappa parameter of our optimizer was set to 2.5, allowing for a balance between exploration and exploitation.\\


The heatmaps below are one of the ten solutions for each permutation of acquisition function and gaussian kernels.




\textbf{Predicted Mean and Variance Plots}\\

\textit{UCB Cost Function with Rational Quadratic Kernel:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ucb_rq.png}\\
\textit{EI Cost Function with Rational Quadratic Kernel:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ei_rq.png}\\
\textit{POI Cost Function with Rational Quadratic Kernel:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{poi_rq.png}\\
\textit{UCB Cost Function with RBF:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ucb_rbf.png}\\
\textit{EI Cost Function with RBF:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ei_rbf.png}\\
\textit{POI Cost Function with RBF:}\\
 \includegraphics[width=\textwidth, height=.3\textheight]{poi_rbf.png}\\

\textbf{Convergence Plots:}\\
In this section, we show the value selected by the bayesian optimizer at each iteration, so that the reader can visualize both the value converged to, as well as the rate of convergence. We define the convergence criteria to have been achieved when the delta between three consecutive iterations is less than 0.01.

We note that in many cases, the exploratory nature of these optimizers will result in predictions which fluctuate wildly back and forth from the current maxima. By requiring three sequential values to have a delta of no less than .01, it is our hope to prevent these fluctuations from interfering with our reported convergence values.\\

The reported values are the iteration of convergence among 10 trials.

\textit{UCB Cost Function with Rational Quadratic Kernel:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ucb_rq_convergence.png}\\
13.8 iterations\\
\textit{EI Cost Function with Rational Quadratic Kernel:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ei_rq_convergence.png}\\
11.9 iterations\\
\textit{POI Cost Function with Rational Quadratic Kernel:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{poi_rq_convergence.png}\\
14.7 iterations\\
\textit{UCB Cost Function with RBF:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ucb_rbf_convergence.png}\\
15.4 iterations\\
\textit{EI Cost Function with RBF:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{ei_rbf_convergence.png}\\
5.9 iterations
\textit{POI Cost Function with RBF:}\\
\includegraphics[width=\textwidth, height=.3\textheight]{poi_rbf_convergence.png}\\
7.4 iterations. \\


In many of the above cases, we observed a strong tendency for the bayesian optimizer to stray from the maximum value. We believe that this is due to our parameter choices, which leads the optimizer to explore different regions of the solution space to avoid becoming stuck at a local maxima.

From our defined convergence criteria, we found that on average, the RBF kernel converged faster than the RQ Kernel, and that the EI acquisition function converged faster than the POI acquisition function, which in turn converged faster than the UCB acquisition function. 

\section{2a}
Generate a time series using a special case of a linear dynamical system that is similar to an autoregressive model in the time series literature.The hidden state of the dynamical system will be a vector with 2 elements, which evolve over time according to:


$$ y_2(t) = y_1(t-1) + \mu_1 $$
$$ y_1(t) = \alpha_1 y_1(t-1) + \alpha_2 y_2 (t-1) + \mu_2 $$

The observation will be a scalar: 

$$ z(t) = y_1(t) + \mu_3 $$

where for i=\{1, 2, 3\}, $\mu_i \sim Normal(0, \sigma^2)$ and you should pick $\{\alpha_1, \alpha_2, \sigma\}$ to obtain interesting dynamics

Plot a couple hundred points that are representative of this process\\
\textbf{Solution:}\\



\section{2b}
Turn your model into a switched linear dynamical system with 3 modes.  Each mode has an associated set of $\{\alpha_1,\alpha_2\}$ parameters. 
Switching is memoryless, i.e., at each time with some small probability $\beta$, the mode m, is re-drawn from the set {1,2,3} with uniform probability.

Draw a graphical model that depicts this switched dynamical system.  Show the conditional distributions on each arc, e.g., write out the transition table for $ P(m(t)  \vert  m(t-1)) $

Generate a nice graph illustrating the switching dynamics.\\
\textbf{Solution:}\\

\includegraphics[width=\textwidth, height=\textheight, keepaspectratio]{slds_diagram.png}\\

\begin{align*}
P(m_{1:t}, h_{1:t}, v_{1:t}) &= \prod_{T}^{t=1} P(v_t \vert h_t, m_t)P(h_t \vert h_{t-1}, m_t)P(m_t \vert m_{t-1})\\
%&= \prod_{T}^{t=1}P(v_t \vert m_t) P(v_t \vert h_t) P(h_t \vert h_{t-1}) P(h_t \vert m_t) P(m_t \vert m_{t-1})
\end{align*} 

Evaluating each of the probabilites in the product above, we find:\\
$$P(v_t \vert h_t, m_t) = \mathcal{N} (v_t \vert v(m_t) + \textbf{B}(m_t)h_t, \Sigma_v(m_t))$$
Where $\textbf{B}(m_t)$ represents the emission matrix for the currently active linear dynamical system. 

$$ P(h_t \vert h_{t-1}, m_t) = \mathcal{N}(h_t \vert h(m_t) + \textbf{A}(m_t) h_t, \Sigma_h(m_t))$$ 

where \textbf{A} is the transition matrix between modes m.\\

and \\
$$ P(m_t \vert m_{t-1}) = \textbf{A}(m_t-1) $$


%$$P(v_t \vert m_t) = \textbf{B}(m_t) + \eta^v(s_t)$$
%where $m_t$ describes which of the set of emission matrices \textbf{B} is active at time t, and $\eta^v(s_t) \sim \mathcal{N}(\eta^v(s_t) \vert \bar{\textbf{v}}, \Sigma_h(s_t))$ describes the observation noise.
%
%$$ P(v_t \vert h_t) =  $$ 
%
%$$ P(h_t \vert h_{t-1}) =  $$ 
%
%$$ P(h_t \vert m_t) =   $$
%
%$$  P(m_t \vert m_{t-1}) = \textbf{B}(m_t) + \eta^v(s_t) $$



%Where $ p(v_t \vert h_t, m_t) = \mathcal{N}(v_t \vert h_t, m_t) + B(m_t)h_t, \Sigma_v(m_t)$ 
%
%and $ p(h_t \vert h_{t-1}, m_t) = \mathcal{N}(h_t \vert \bar h_t(m_t) + A(m_t) h_t, \Sigma_h(m_t))   $



\section{2c}
Now try to perform inference using the data set you generated in either part 2a (easy option) or part 2b (harder option).   

If you use the data set from part 2a, then you can model it with a Kalman filter. Implement a KF and plot a portion of the time series along with the posterior predictive distribution. The posterior predictive distribution is P(z(t+1) | z(1), ..., z(t)). Since this distribution is Gaussian, plot the mean and some representation of uncertainty (e.g., lines at +/- 2 SD, or the shaded representation one sometimes sees.)

If you use the data set from part 2b, you could be ambitious and use a particle filter to perform inference, in which case you'll obtain a set of samples that represent the posterior predictive distribution. Or you could try using a model that isn't quite up to the task, e.g., an HMM with a Gaussian observation model (i.e., each state of the HMM outputs a constant value corrupted by Gaussian noise). You'd have to train the HMM, and it would require more than 3 states, since each state represents a constant level, but it would be interesting to see how poorly it models the data. \\

\textbf{Solution:}\\



\pagebreak



\end{document}

