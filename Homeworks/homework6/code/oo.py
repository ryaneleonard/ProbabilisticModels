import pandas as pd
import pymc3 as pm
import numpy as np

model = pm.Model()

with model:
    ## p(G1)
    G1 = pm.Bernoulli('G1', 0.5)
    ## p(G2|G1)
    G2_p = pm.Deterministic('G2_p', pm.math.switch(G1, 0.9, 0.1))
    G2 = pm.Bernoulli('G2', G2_p)
    ## p(G3|G1)
    G3_p = pm.Deterministic('G3_p', pm.math.switch(G1, 0.9, 0.1))
    G3 = pm.Bernoulli('G3', G3_p)

    X2 = pm.Normal('X2', mu=50, sd=3.1622, observed=G2_p)

    X3 = pm.Normal('X3', mu=50, sd=3.1622, observed=G3_p)

    step = pm.Metropolis()
    trace = pm.sample(50000, step=step, tune=50, random_seed=123, progressbar=True)  #

# pm.traceplot(trace) #plot histogram

print (G1.logp({'X2':50,'X3':50,'G1':1,'G2':2,'G3':1}))
print (X3.logp({'X2':50,'X3':50,'G1':1,'G2':1,'G3':1}))
