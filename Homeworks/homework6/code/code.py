import pymc3 as pm
import numpy as np
import pandas as pd

num_samples = 50000
tune=1000
model = pm.Model()

with model:
    G1 = pm.Bernoulli('G1',0.5)
    G2_p = pm.Deterministic('G2_p',pm.math.switch(G1,0.1,0.9))
    G2 = pm.Bernoulli('G2',G2_p)
    G3_p = pm.Deterministic('G3_p',pm.math.switch(G1,0.1,0.9))
    G3 = pm.Bernoulli('G3',G3_p)
    X2 = pm.Normal('X2',mu=pm.math.switch(G2,50,60),sd=np.sqrt(10))
    X3 = pm.Normal('X3',mu=pm.math.switch(G3,50,60),sd=np.sqrt(10))
    trace = pm.sample(num_samples, tune=tune)

dictionary = {
            'G1': [ii for ii in trace['G1'].tolist()],
            'G2': [ii for ii in trace['G2'].tolist()],
            'G3': [ii for ii in trace['G3'].tolist()],
            'X2': [ii for ii in trace['X2'].tolist()],
            'X3': [ii for ii in trace['X3'].tolist()],
            }

df=pd.DataFrame(dictionary)

p_g_x2 = (df[(df['G1'] == 1) & (df['X2'] < 50.5) & (df['X2'] > 49.5)].shape[0]) / (df[(df['X2'] < 50.5) & (df['X2'] > 49.5)].shape[0])

p_x3_x2 = (df[(df['X2'] < 50.5) & (df['X2'] > 49.5) & (df['X3'] < 50.5) & (df['X3'] > 49.5)].shape[0]) /  (df[(df['X2'] < 50.5) & (df['X2'] > 49.5)].shape[0])


print(p_g_x2)
print(p_x3_x2)
